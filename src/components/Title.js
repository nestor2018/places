import React, { Component } from 'react';

class Title extends Component {
  render() {
    return(
      <div className="title">
        <h1>Places</h1>
        <p>Descubre lugares de manera simple</p>
      </div>
    );
  }
}

export default Title;