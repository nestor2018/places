import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import Title from '../components/Title';
import data from '../requests/places';

import '../scss/styles.scss';
import typography from 'material-ui/styles';

class Home extends Component {
  
  constructor(props){
    super(props);
  }

  places(){
    return data.places.map(places=>{
      return(
          <Card>
            <CardMedia>
              <img 
                src={places.imageUrl}
                className="place-img"
              />
            </CardMedia>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">{places.title}</Typography>
              <typography component="p">{places.description}</typography>
            </CardContent>
          </Card>
      );
    })
  }

  render() {
    return(
      <div className="header">
        <div className="header-background">
          <div className="container">
            <div className="header-main">
              <Title/>
              <Button variant="contained" color="secondary">Crear cuenta gratuita</Button>
              <img 
              className="header-illustration"
              src="./src/images/map.png"
              />
            </div>
            <div>
              <ul className="cards">
                <Card className="header-Benefit">
                  <CardContent className="cardContent">
                    <div className="benefit">
                      <div className="header-Benefit-corazon">
                        <img
                          src="./src/images/corazon.png"
                        />
                      </div>
                    <div className="header-Benefit-content">
                      <h3>Calificaciones con emociones</h3>
                      <p>Califica tus lugares con experiencias no con titulos</p>
                    </div>
                      </div>
                  </CardContent>
                </Card>
                <Card className="header-Benefit">
                  <CardContent className="cardContent">
                    <div className="benefit">
                      <div className="header-Benefit-internet">
                        <img
                        src="./src/images/internet.png"
                        />
                      </div>
                      <div className="header-Benefit-content">
                        <h3>¿Sin Internet? Sin problemas</h3>
                        <p>Places funciona sin internet o conexiones lentas</p>
                      </div>
                    </div>
                  </CardContent>
                </Card>
                <Card className="header-Benefit">
                  <CardContent className="cardContent">
                    <div className="benefit">
                      <div className="header-Benefit-estrella">
                        <img
                        src="./src/images/estrella.png"
                        />
                      </div>
                      <div className="header-Benefit-content">
                        <h3>Tus lugares favoritos</h3>
                        <p>Define tu lista de sitios favoritos</p>
                      </div>
                    </div>
                  </CardContent>
                </Card>
              </ul>
            </div>
          </div>
        </div>
          <div className="contenedor-cards">
            <h3>Sitios populares</h3>
            <div className="place">
              {this.places()}
            </div>
          </div>
      </div>
    );
  }
}

export default Home;