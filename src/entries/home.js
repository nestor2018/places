import React from 'react';
import { render } from 'react-dom';

import Home from '../pages/home';

const app = document.getElementById('app')

render(<Home/>, app)